<?php

namespace app\controllers;

use app\models\Users;
use Yii;
use yii\web\Controller;

class SiteController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        $model = new Users();

        if($model->load(Yii::$app->request->post(), '') && $model->validate()){
            $model->save(false);
            Yii::$app->session->setFlash('success', 'Успешно сохранено');
        } else {
            foreach ($model->errors as $attribute){
                foreach ($attribute as $error){
                    Yii::$app->session->setFlash('danger', $error);
                }
            }
        }


        return $this->render('index', ['model' => $model]);
    }
}