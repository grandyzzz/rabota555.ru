<?php

?>
<?php $this->beginContent('@app/views/layouts/base.php'); ?>
    <div id="wrapper">
        <main>
            <?= $content ?>
        </main>
    </div>
<?php $this->endContent(); ?>