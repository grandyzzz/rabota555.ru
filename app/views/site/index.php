<?php
use app\models\City;
use app\models\Katalogv;
use yii\bootstrap\Alert;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \app\models\Users $model
 */

$this->title = 'Анкета на вакансию';
?>

<div class="container">
    <div class="smart-wrap">
        <div class="smart-forms smart-container wrap-0">

            <div class="form-header header-primary">
                <h4><i class="fa fa-pencil-square"></i>Анкета на вакансию</h4>
            </div><!-- end .form-header section -->
            <p align="center"><span style="color: rgb(255, 0, 0);"></span></p>

            <?php foreach (Yii::$app->session->getAllFlashes() as $type => $message) : ?>
                <?php if (in_array($type, ['success', 'danger', 'warning', 'info'])): ?>
                    <?= Alert::widget([
                        'options' => ['class' => 'alert-dismissible alert-' . $type],
                        'body' => $message
                    ]) ?>
                <?php endif ?>
            <?php endforeach; ?>

            <form method="post" action="" id="account">
                <?= Html::hiddenInput(Yii::$app->request->csrfParam, Yii::$app->request->csrfToken)?>
                <div class="form-body">
                    <table width="100%" border="0"><tr><td width="50%" valign="top" style="padding-right: 30px;">
                                <label for="names" class="field-label">Представьтесь</label>
                                <div class="frm-row">

                                    <div class="section colm colm12">
                                        <label class="field prepend-icon">
                                            <input type="text" required name="lastname" id="fname" class="gui-input" placeholder="Имя..." value="<?=$model->firstname?>">
                                            <span class="field-icon"><i class="fa fa-user"></i></span>
                                        </label>
                                    </div><!-- end section -->

                                    <div class="section colm colm12">
                                        <label class="field prepend-icon">
                                            <input type="text" name="firstname" id="lastname" class="gui-input" placeholder="Фамилия..."value="<?=$model->lastname?>">
                                            <span class="field-icon"><i class="fa fa-user"></i></span>
                                        </label>
                                    </div><!-- end section -->


                                </div><!-- end frm-row section -->


                                <!-- end .frm-row section -->
                                <div class="section">
                                    <label for="email" class="field-label">Возраст</label>
                                    <label class="field prepend-icon">
                                        <input type="age" name="age" id="age" class="gui-input" placeholder="Сколько лет?..."value="<?=$model->age?>">
                                    </label>
                                </div><!-- end section -->
                                <div class="section">
                                    <label for="location" class="field-label" value="">Гражданство </label>
                                    <label class="field select">
                                        <select id="location" name="location">
                                            <option value="">Выбирете страну...</option>
                                            <? foreach (City::find()->orderBy('id_city')->all() as $city): ?>
                                                <option value="<?= $city->name_city ?>" <?= $model->location == $city->name_city ? 'selected' : ''?> ><?= $city->name_city ?></option>
                                            <? endforeach; ?>

                                        </select>
                                        <i class="arrow double"></i>
                                    </label>
                                </div><!-- end section -->
                                <div class="section">
                                    <label for="gender" class="field-label">Пол </label>
                                    <label class="field select">
                                        <select id="gender" name="gender">
                                            <option value="">Выбирете пол...</option>
                                            <option value="Мужчина" <?= $model->gender == 'Мужчина' ? 'selected' : ''?>>Мужчина</option>
                                            <option value="Женщина" <?= $model->gender == 'Женщина' ? 'selected' : ''?>>Женщина</option>

                                        </select>
                                        <i class="arrow double"></i>
                                    </label>
                                </div><!-- end section -->
                            </td>
                            <td width="50%" valign="top">
                                <div class="section">
                                    <label for="email" class="field-label">Ваш email адрес</label>
                                    <label class="field prepend-icon">
                                        <input type="email" name="email" id="email" class="gui-input" placeholder="example@google.com..." value="<?=$model->email?>">
                                        <span class="field-icon"><i class="fa fa-envelope"></i></span>
                                    </label>
                                </div><!-- end section -->
                                <div class="section">
                                    <label for="mobile" class="field-label">Телефон </label>
                                    <label class="field prepend-icon">
                                        <input type="tel" name="telephone" id="mobile" class="gui-input" placeholder="+7" value="<?=$model->telephone?>">
                                        <span class="field-icon"><i class="fa fa-phone-square"></i></span>
                                    </label>
                                </div><!-- end section -->

                                <div class="section">
                                    <label for="dolj" class="field-label">Претендую на должность </label>
                                    <label class="field select">
                                        <select id="dolj" name="dolj">
                                            <option value="">Должность...</option>
                                            <? foreach(Katalogv::find()->orderBy('id_katalogv')->all() as $activity): ?>
                                                <option value="<?= $activity->name_katalogv ?>" <?= $model->dolj == $activity->name_katalogv ? 'selected' : ''?>><?= $activity->name_katalogv ?></option>
                                            <? endforeach; ?>
                                        </select>
                                        <i class="arrow double"></i>
                                    </label>
                                </div><!-- end section -->

                                <div>
                                    <label for="district" class="field-label">Станция метро:  </label>
                                    <label class="field prepend-icon">
                                        <input id="tags" name="metro" class="gui-input" value="<?=$model->metro?>">
                                    </label>
                                </div><!-- end section -->

                                <div class="section" style="margin-top: 19px;">
                                    <label class="option">
                                        <input type="checkbox" name="check1">
                                        <span class="checkbox"></span>
                                        Я соглашаюсь на обработку моих персональных данных
                                    </label>
                                </div><!-- end section -->
                </div><!-- end .form-body section -->
                <div class="form-footer">
                    <button type="submit" class="button btn-primary">Отправить заявку</button>
                </div><!-- end .form-footer section -->
                </td>
                </tr>
                </table>
            </form>

        </div><!-- end .smart-forms section -->
    </div>
</div>
