<?php
$this->title = Yii::t('easyii/jobs', 'Edit text');
?>
<?= $this->render('_menu') ?>
<?= $this->render('_form', ['model' => $model]) ?>