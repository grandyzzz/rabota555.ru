<?php
namespace app\modules\employees;

class EmployeesModule extends \yii\easyii\components\Module
{
    public $layout = '@vendor/grozzzny/adminlte_easyiicms/views/layouts/main';

    public $settings = [
        'mailAdminOnNewOrder' => true,
        'subjectOnNewOrder' => 'New order',
        'templateOnNewOrder' => '@app/modules/employees/mail/en/new_order',
        'subjectNotifyUser' => 'Your order status changed',
        'templateNotifyUser' => '@app/modules/employees/mail/en/notify_user',
        'frontendShopcartRoute' => '/employees/order',
        'enablePhone' => true,
        'enableEmail' => true
    ];

    public static $installConfig = [
        'title' => [
            'en' => 'Orders',
            'ru' => 'Заказы',
        ],
        'icon' => 'shopping-cart',
        'order_num' => 120,
    ];
}