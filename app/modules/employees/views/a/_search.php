<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UsersSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'date_reg') ?>

    <?= $form->field($model, 'firstname') ?>

    <?= $form->field($model, 'lastname') ?>


    <?php  echo $form->field($model, 'age') ?>

    <?php  echo $form->field($model, 'gender')->dropDownList(['Мужской', 'Женский']) ?>

    <?php  echo $form->field($model, 'location') ?>

    <?php  echo $form->field($model, 'email') ?>

    <?php  echo $form->field($model, 'telephone') ?>

    <?php  echo $form->field($model, 'dolj') ?>

    <?php  echo $form->field($model, 'district') ?>

    <?php  echo $form->field($model, 'metro') ?>

    <?php  echo $form->field($model, 'status') ?>

    <?php  echo $form->field($model, 'manager') ?>

    <?php  echo $form->field($model, 'comments') ?>

    <?php  echo $form->field($model, 'object_name') ?>

    <?php  echo $form->field($model, 'object_addr') ?>

    <div class="form-group">
        <?= Html::submitButton('Поиск', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Сбросить', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
