<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Сотрудники';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php  //echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать пользователя', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'date_reg',
                'filter' => \yii\easyii\widgets\DateTimePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'date_reg'
                ])
            ],
            'firstname',
            'lastname',
            'age',
            [
                'attribute' => 'gender',
                'filter' => [
                    'empty' => 'Не определен',
                    'Мужчина' => 'Мужчина',
                    'Женщина' => 'Женщина'
                ],
                'filterInputOptions' => ['class' => 'form-control']
            ],
            'location',
            'email:email',
            'telephone',
            'dolj',
            [
                'attribute' => 'district',
                'filter' => [
                    'empty' => 'Не определен',
                    'Мужчина' => 'Мужчина',
                    'Женщина' => 'Женщина'
                ],
                'filterInputOptions' => ['class' => 'form-control']
            ],
            'metro',
            'status:ntext',
            'manager',
            'comments:ntext',
            'object_name',
            'object_addr',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
