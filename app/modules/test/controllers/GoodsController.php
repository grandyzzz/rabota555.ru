<?php
namespace app\modules\test\controllers;

use Yii;

use yii\easyii\components\Controller;
use app\modules\test\models\Good;

class GoodsController extends Controller
{
    public function actionDelete($id)
    {
        if(($model = Good::findOne($id))){
            $model->delete();
        } else {
            $this->error = Yii::t('easyii', 'Not found');
        }
        return $this->formatResponse(Yii::t('easyii/test', 'Order deleted'));
    }
}