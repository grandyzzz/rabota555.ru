<?php

namespace app\modules\metro\models;

use Yii;

/**
 * This is the model class for table "re_metro".
 *
 * @property int $id_re_metro
 * @property string $name_re_metro
 * @property int $city_id
 * @property string $region
 * @property string $seen
 */
class Metro extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 're_metro';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id'], 'integer'],
            [['region', 'seen'], 'required'],
            [['seen'], 'string'],
            [['name_re_metro'], 'string', 'max' => 255],
            [['region'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_re_metro' => 'Id Re Metro',
            'name_re_metro' => 'Название метро',
            'city_id' => 'City ID',
            'region' => 'Region',
            'seen' => 'Seen',
        ];
    }
}
