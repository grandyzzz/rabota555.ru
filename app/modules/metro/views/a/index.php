<?php
use yii\helpers\Url;

$this->title = 'Метро и районы';

$module = $this->context->module->id;
?>

<?= $this->render('_menu') ?>

<?php if($data->count > 0) : ?>
    <table class="table table-hover">
        <thead>
        <tr>
            <th>ID</th>
            <th>Метро</th>
            <th>Район</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($data->models as $item) : ?>
            <tr>
                <td><?= $item->id_re_metro ?></td>
                <td><a href="<?= Url::to(['/admin/'.$module.'/a/edit', 'id' => $item->id_re_metro]) ?>"><?= $item->name_re_metro ?></a></td>
                <td><a href="<?= Url::to(['/admin/'.$module.'/a/edit', 'id' => $item->id_re_metro]) ?>"><?= $item->region ?></a></td>
                <td><a href="<?= Url::to(['/admin/'.$module.'/a/delete', 'id' => $item->id_re_metro]) ?>" class="glyphicon glyphicon-remove confirm-delete" title="<?= Yii::t('easyii', 'Delete item') ?>"></a></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <?= yii\widgets\LinkPager::widget([
        'pagination' => $data->pagination
    ]) ?>
<?php else : ?>
    <p><?= Yii::t('easyii', 'No records found') ?></p>
<?php endif; ?>