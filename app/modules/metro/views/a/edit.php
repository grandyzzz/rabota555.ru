<?php
$this->title = Yii::t('easyii/metro', 'Edit text');
?>
<?= $this->render('_menu') ?>
<?= $this->render('_form', ['model' => $model]) ?>