<?php
use yii\helpers\Url;

$this->title = 'Анкеты на карте';
//$this->registerJsFile('@web/js/bundle.js');

$module = $this->context->module->id;
?>

<div  id="map" style="width: 100%; height: 700px"></div>


<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script type="text/javascript" src="http://kashey.ru/maps/regions.js?4"></script>

<script type="text/javascript">
    var myMap, objectManager;

    ymaps.ready(init);

    function init () {

        myMap = new ymaps.Map('map', {
            center: [55.76, 37.64], // Москва
            zoom: 10
        });

        objectManager = new ymaps.ObjectManager({
            // Чтобы метки начали кластеризоваться, выставляем опцию.
            clusterize: true,
            // ObjectManager принимает те же опции, что и кластеризатор.
            gridSize: 32,
            clusterDisableClickZoom: true
        });

        // Чтобы задать опции одиночным объектам и кластерам,
        // обратимся к дочерним коллекциям ObjectManager.
        objectManager.objects.options.set('preset', 'islands#greenDotIcon');
        objectManager.clusters.options.set('preset', 'islands#greenClusterIcons');


        var dataCollection = {
            type: 'FeatureCollection',
            features: [{
                type: 'Feature',
                id: 1,
                geometry: {
                    type: 'Point',
                    coordinates: [55.76, 37.64]
                },
                properties: {
                    hintContent: 'Текст всплывающей подсказки',
                    balloonContent: 'Содержимое балуна'
                }
            }

            ]
        }

        objectManager.add(dataCollection);

        myMap.geoObjects.add(objectManager);



        var geoMap=myMap,
            collection=0;
        osmeRegions.geoJSON(102269, {
            lang: 'ru',
            quality:0
//            postFilter: function(region){
//                return region.osmId==102269;
//            },
//            scheme: {
//                102269:function (region){
//                    var oid=region.osmId;
//                    return region.hasBorderWith(2162196);
//                }
           // }
        }, function (data, pure) {







            data.features.forEach(function (item, i, arr) {

                osmeRegions.geoJSON(item.properties.osmId, {
                        lang: 'ru',
                        quality: 0
                    }, function (districts, pure) {

                    console.log(districts.features);




                    collection = osmeRegions.toYandex(districts, ymaps);
                    setStyles(collection);
                    collection.add(geoMap);



                    geoMap.setBounds(collection.collection.getBounds(), {duration: 300});

                    }
                );
            });

        });
    }
    function setStyles(collection) {
        collection.setStyles(function (object) {
            return ({
                strokeWidth: 1,
                strokeColor: "666",
                fillColor: 'EEE',
                visible: true,
                opacity: 0.4
            });
        });
    }
</script>

