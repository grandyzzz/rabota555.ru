<?php
$this->title = Yii::t('easyii/map', 'Edit text');
?>
<?= $this->render('_menu') ?>
<?= $this->render('_form', ['model' => $model]) ?>