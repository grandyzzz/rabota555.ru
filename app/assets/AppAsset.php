<?php
namespace app\assets;

class AppAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@app/media';
    public $css = [
        //'css/normalize.css',
        'css/styles.css',
        'css/custom.css',
        'css/smart-addons.css',
        'css/smart-forms.css',
        'css/smart-forms-ie8.css',
    ];
    public $js = [
        'js/scripts.js',
        'js/jquery.placeholder.min.js',
        'js/bundle.js',
        'https://use.fontawesome.com/f0716e379e.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];
}
