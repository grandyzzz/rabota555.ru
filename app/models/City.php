<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "city".
 *
 * @property int $id_city
 * @property string $name_city
 * @property string $default_
 * @property string $seen
 * @property string $pass
 */
class City extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'city';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['default_', 'seen', 'pass'], 'string'],
            [['name_city'], 'string', 'max' => 100],
            [['name_city'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_city' => 'Id City',
            'name_city' => 'Название города',
            'default_' => 'Default',
            'seen' => 'Seen',
            'pass' => 'Pass',
        ];
    }
}
