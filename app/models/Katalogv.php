<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "katalogv".
 *
 * @property int $id_katalogv
 * @property string $name_katalogv
 * @property string $default_
 * @property string $seen
 * @property string $pass
 */
class Katalogv extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'katalogv';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['default_', 'seen', 'pass'], 'string'],
            [['name_katalogv'], 'string', 'max' => 100],
            [['name_katalogv'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_katalogv' => 'Id Katalogv',
            'name_katalogv' => 'Название',
            'default_' => 'Default',
            'seen' => 'Seen',
            'pass' => 'Pass',
        ];
    }
}
