<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Users;

/**
 * UsersSearch represents the model behind the search form of `app\models\Users`.
 */
class UsersSearch extends Users
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'age'], 'integer'],
            [['date_reg', 'firstname', 'lastname', 'patronymic', 'date_row', 'gender', 'location', 'email', 'telephone', 'dolj', 'district', 'metro', 'status', 'manager', 'comments', 'object_name', 'object_addr'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Users::find()->orderBy(['id_user' => SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize' => 300]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_user' => $this->id_user,
            'date_reg' => $this->date_reg,
            'date_row' => $this->date_row,
            'age' => $this->age,
        ]);

        $query->andFilterWhere(['like', 'firstname', $this->firstname])
            ->andFilterWhere(['like', 'lastname', $this->lastname])
            ->andFilterWhere(['like', 'location', $this->location])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'telephone', $this->telephone])
            ->andFilterWhere(['like', 'dolj', $this->dolj])
            ->andFilterWhere(['like', 'district', $this->district])
            ->andFilterWhere(['like', 'metro', $this->metro])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'manager', $this->manager])
            ->andFilterWhere(['like', 'comments', $this->comments])
            ->andFilterWhere(['like', 'object_name', $this->object_name])
            ->andFilterWhere(['like', 'object_addr', $this->object_addr]);

        if($this->gender == 'empty'){
            $query->andWhere(['gender' => '']);
        } else {
            $query->andFilterWhere(['like', 'gender', $this->gender]);
        }

        return $dataProvider;
    }
}
