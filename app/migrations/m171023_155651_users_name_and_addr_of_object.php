<?php

use yii\db\Migration;

/**
 * Class m171023_155651_users_name_and_addr_of_object
 */
class m171023_155651_users_name_and_addr_of_object extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171023_155651_users_name_and_addr_of_object cannot be reverted.\n";

        return false;
    }


    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->addColumn('users','object_name', $this->string());
        $this->addColumn('users','object_addr', $this->string());
    }

    public function down()
    {
        $this->dropColumn('users','object_name');
        $this->dropColumn('users','object_addr');
    }

}
